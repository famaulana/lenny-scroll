import { useEffect, useRef, useState } from "react";
import {
  useScroll,
  motion,
  useTransform,
  useMotionValueEvent,
} from "framer-motion";
import useDimensions from "@/hooks/dimension";

const CardXScrollingParalax = () => {
  const containerXCardParalax = useRef(null);
  const [scrollX, setScrollX] = useState(0);

  const { height, width } = useDimensions();

  const { scrollYProgress } = useScroll({
    target: containerXCardParalax,
    offset: ["start start", "end end"],
  });

  // const y = useTransform(scrollYProgress, [0, 1], [0, height * 3.595]);

  useMotionValueEvent(scrollYProgress, "change", (latest) => {
    let position;
    let item = containerXCardParalax.current?.children[0];

    for (let i = 0; i < item.children[0].children.length; i++) {
      const threshold = (i + 1) * (1 / (item.children[0].children.length - 1));

      if (latest < threshold) {
        position = i;
        break;
      }

      // console.log(threshold);
      // console.log(latest);
    }

    // Check how many window width can show cards
    let posibilityScrollingWidth = item.children[0].scrollWidth;

    let totalCardShows =
      width / (item.children[0].children[0].offsetWidth + 15);
    let travelPerCard =
      position * (item.children[0].children[0].offsetWidth + 15);

    if (
      travelPerCard >
      posibilityScrollingWidth -
        item.children[0].children[0].offsetWidth * totalCardShows
    ) {
      setScrollX(
        item.children[0].scrollWidth -
          (totalCardShows * item.children[0].children[0].offsetWidth + 15 * 2)
      );
    } else {
      setScrollX(travelPerCard);
    }
  });

  useEffect(() => {
    let itemWidth = containerXCardParalax.current?.children[0];

    containerXCardParalax.current.style.height = `${itemWidth.children[0].scrollWidth}px`;
  }, []);

  return (
    <section
      ref={containerXCardParalax}
      className="flex flex-row h-min-screen justify-top relative border border-dashed border-teal-400 pt-[30vh] pb-[10vh]"
    >
      <div
        className="flex h-fit w-full overflow-x-hidden"
        style={{ position: "sticky", top: "calc(50vh - calc(200px/2))" }}
      >
        <motion.div
          className="flex flex-row grow gap-[15px] h-fit"
          animate={{
            x: -scrollX,
          }}
        >
          <div className="ms-[15px] min-w-[600px] h-[200px] text-center flex justify-center border border-dashed border-sky-500 rounded">
            <p className="my-auto">1</p>
          </div>
          <div className="min-w-[600px] h-[200px] text-center flex justify-center border border-dashed border-sky-500 rounded">
            <p className="my-auto">2</p>
          </div>
          <div className="min-w-[600px] h-[200px] text-center flex justify-center border border-dashed border-sky-500 rounded">
            <p className="my-auto">3</p>
          </div>
          <div className="min-w-[600px] h-[200px] text-center flex justify-center border border-dashed border-sky-500 rounded">
            <p className="my-auto">4</p>
          </div>
          <div className="min-w-[600px] h-[200px] text-center flex justify-center border border-dashed border-sky-500 rounded">
            <p className="my-auto">5</p>
          </div>
          <div className="me-[15px] min-w-[600px] h-[200px] text-center flex justify-center border border-dashed border-sky-500 rounded">
            <p className="my-auto">6</p>
          </div>
        </motion.div>
      </div>
    </section>
  );
};

export default CardXScrollingParalax;
