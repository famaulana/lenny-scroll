import Styles from "./index.module.scss";
import { useState, useRef, useEffect } from "react";
import {
  useScroll,
  useMotionValueEvent,
  motion,
  useTransform,
} from "framer-motion";
import DropletsAnimation from "./zoomSupport/DropletsAnimation";
import useDimensions from "@/hooks/dimension";

const masking = {
  initial: {
    height: "0vw",
    width: "0vw",
    borderRadius: "50pc",
    transition: {
      duration: 0.5,
    },
  },
  enter: (i) => ({
    height: "100vw",
    width: "100vw",
    borderRadius: "0pc",
    transition: {
      duration: 0.5,
    },
  }),
};

const container = {
  initial: {
    background: "transparent",
  },
  enter: {
    background: "white",
  },
};

const ZoomSectionParalax = () => {
  const containerContentParalax = useRef(null);
  const [yOffset, setYOffset] = useState(0);
  const [yTopOffset, setYTopOffset] = useState(0);
  const [yProgress, setYProgress] = useState(0);
  const [enterAnimation, setEnterAnimation] = useState(false);
  const [exitAnimation, setExitAnimation] = useState(false);

  const { height } = useDimensions();

  const { scrollY, scrollYProgress } = useScroll({
    target: containerContentParalax,
    offset: ["start center", "end start"],
  });

  useMotionValueEvent(scrollY, "change", (latest) => {
    setYOffset(latest);
  });

  useMotionValueEvent(scrollYProgress, "change", (latest) => {
    if (yTopOffset == 0) {
      setYTopOffset(yOffset);
    }

    if (latest > 0) {
      setEnterAnimation(true);
    } else {
      setEnterAnimation(false);
    }
    setYProgress(latest);

    if (yOffset - yTopOffset >= height / 2 && latest < 1) {
      setExitAnimation(true);
    } else if (latest == 1) {
      setExitAnimation(true);
    } else {
      setExitAnimation(false);
    }
  });

  useEffect(() => {
    console.log(exitAnimation);
  }, [exitAnimation]);

  return (
    <section className="border border-dashed border-teal-400">
      <DropletsAnimation />
      <motion.div
        ref={containerContentParalax}
        className={`flex justify-center ${Styles.containerContent}`}
        animate={exitAnimation ? container.enter : container.initial}
      >
        <motion.div
          className={`${Styles.masking} ${exitAnimation ? "hidden" : ""}`}
          custom={yProgress}
          animate={enterAnimation ? masking.enter : masking.initial}
        />
        <div
          className={`${Styles.wrapper} ${exitAnimation ? Styles.show : ""}`}
        >
          {/* <p>Thanks for visiting this sites</p> */}
        </div>
      </motion.div>
    </section>
  );
};

export default ZoomSectionParalax;
