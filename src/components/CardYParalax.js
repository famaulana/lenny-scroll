import Image from "next/image";
import useDimensions from "@/hooks/dimension";
import { useRef } from "react";
import { useScroll, motion, useTransform } from "framer-motion";

const CardYParalax = () => {
  const containerYCardParalax = useRef(null);

  const { height } = useDimensions();

  // Panning the containerYCardParalax for target trigger
  const { scrollYProgress } = useScroll({
    target: containerYCardParalax,
    offset: ["start end", "end start"],
  });

  // Moving on scroll div of child containerYCardParalax
  const y1 = useTransform(scrollYProgress, [0, 1], [0, height * 0.3]);
  const y2 = useTransform(scrollYProgress, [0, 1], [0, height * 0.7]);
  const y3 = useTransform(scrollYProgress, [0, 1], [0, height * 0.5]);
  const y4 = useTransform(scrollYProgress, [0, 1], [0, height * 1]);

  return (
    <section
      ref={containerYCardParalax}
      className="flex flex-row grow w-sceen h-screen justify-center gap-5 border border-dashed border-teal-400 px-5 overflow-hidden"
    >
      <motion.div
        className="flex flex-col grow gap-5 mt-[-500px]"
        style={{ y: y1 }}
      >
        <CardDummy id={Math.floor(Math.random() * 100)} />
        <CardDummy id={Math.floor(Math.random() * 100)} />
        <CardDummy id={Math.floor(Math.random() * 100)} />
      </motion.div>
      <motion.div
        className="flex flex-col grow gap-5 mt-[-500px]"
        style={{ y: y2 }}
      >
        <CardDummy id={Math.floor(Math.random() * 100)} />
        <CardDummy id={Math.floor(Math.random() * 100)} />
        <CardDummy id={Math.floor(Math.random() * 100)} />
      </motion.div>
      <motion.div
        className="flex flex-col grow gap-5 mt-[-500px]"
        style={{ y: y3 }}
      >
        <CardDummy id={Math.floor(Math.random() * 100)} />
        <CardDummy id={Math.floor(Math.random() * 100)} />
        <CardDummy id={Math.floor(Math.random() * 100)} />
      </motion.div>
      <motion.div
        className="flex flex-col grow gap-5 mt-[-500px]"
        style={{ y: y4 }}
      >
        <CardDummy id={Math.floor(Math.random() * 100)} />
        <CardDummy id={Math.floor(Math.random() * 100)} />
        <CardDummy id={Math.floor(Math.random() * 100)} />
      </motion.div>
    </section>
  );
};

export default CardYParalax;

const CardDummy = (id) => {
  return (
    <div
      className="loadImage w-full min-h-[500px] text-center flex justify-center border border-dashed border-sky-500 rounded relative"
      style={{
        backgroundImage: `url('https://picsum.photos/id/${id.id}/40/60')`,
        objectFit: "cover",
        objectPosition: "center",
        backgroundSize: "100% 100%",
        backgroundRepeat: "no-repeat",
      }}
    >
      <Image
        src={`https://picsum.photos/id/${id.id}/400/600`}
        fill={true}
        sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
        alt="Card Image"
        loading="lazy"
        onLoad={(e) => e.target.parentElement.classList.remove("loadImage")}
      />
    </div>
  );
};
