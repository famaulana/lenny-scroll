"use client";
import useDimensions from "@/hooks/dimension";
import { useRef, useState } from "react";
import {
  useScroll,
  motion,
  useTransform,
  useMotionValueEvent,
} from "framer-motion";
import Styles from "./index.module.scss";

const appearing = {
  initial: {
    opacity: 0,
    scale: 0.5,
  },
  enter: {
    opacity: 1,
    scale: 2,
  },
};

const zooming = {
  out: {
    opacity: 0,
    scale: 0,
    rotateX: "80deg",
  },
  initial: (i) => ({
    opacity: i < 0.1 ? 0 : 1,
    scale: 1,
    rotateX: "80deg",
  }),
  waiting: {
    opacity: 1,
    scale: 1,
    rotateX: "0deg",
    y: "-17vh",
  },
  enter: (i) => ({
    opacity: 1,
    rotateX: "0deg",
    scale: i * 50,
    y: "-17vh",
  }),
};

const DropletsAnimation = () => {
  const [enterSection, setEnterSection] = useState(false);
  const [waitingSection, setWaitingSection] = useState(false);
  const [openSection, setOpenSection] = useState(false);
  const [anchorY, setAnchorY] = useState(null);
  const [anchorYProgress, setAnchorYProgress] = useState(null);
  const containerZooomParalax = useRef(null);

  const [initialHeightSection, setInitialHeightSection] = useState(null);

  const { height } = useDimensions();

  // Panning the containerZooomParalax for target trigger
  const { scrollY, scrollYProgress } = useScroll({
    target: containerZooomParalax,
    offset: ["start center", "end start"],
  });

  // Moving on scroll div of child containerZooomParalax
  const y1 = useTransform(scrollYProgress, [0, 1], [0, height * 3]);

  useMotionValueEvent(scrollY, "change", (latest) => {
    setAnchorY(latest);
  });

  useMotionValueEvent(scrollYProgress, "change", (latest) => {
    setAnchorYProgress(latest);

    if (latest <= 0.01) {
      setInitialHeightSection(scrollY.current);
      setEnterSection(false);
      setWaitingSection(false);
    }

    if (anchorY - initialHeightSection >= height / 2) {
      setEnterSection(false);
      if (anchorY - initialHeightSection >= height / 1.5) {
        setWaitingSection(false);
        setOpenSection(true);
      }

      if (anchorY - initialHeightSection <= height / 1.5) {
        setWaitingSection(true);
      } else {
        setWaitingSection(false);
      }
    } else if (latest >= 0.01) {
      setEnterSection(true);
      setOpenSection(false);
    }
  });

  return (
    <div
      ref={containerZooomParalax}
      className={`flex flex-row grow w-sceen justify-center gap-5 px-5 relative  ${Styles.section}`}
    >
      <motion.div className={Styles.containerDroplets} style={{ y: y1 }}>
        <div className={Styles.containerWater}>
          <motion.div
            className={Styles.water}
            animate={enterSection ? appearing.enter : appearing.initial}
          />
        </div>
      </motion.div>
      <CircleAnimation
        waiting={waitingSection}
        enter={enterSection}
        open={openSection}
        anchorY={anchorYProgress}
      />
    </div>
  );
};

export default DropletsAnimation;

const CircleAnimation = (props) => {
  return (
    <motion.div
      className={`w-10 h-10 bg-white rounded-full ${Styles.circle}`}
      custom={props.anchorY}
      animate={
        props.open
          ? zooming.enter
          : props.enter
          ? zooming.initial
          : props.waiting
          ? zooming.waiting
          : zooming.out
      }
    ></motion.div>
  );
};
