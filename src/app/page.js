"use client";
import { useEffect } from "react";
import Lenis from "@studio-freight/lenis";
import CardYParalax from "@/components/CardYParalax";
import CardXScrollingParalax from "@/components/CardXScrollingParalax";
import ZoomSectionParalax from "@/components/ZoomSectionParalax";

export default function Home() {
  useEffect(() => {
    const lenis = new Lenis();

    function raf(time) {
      lenis.raf(time);
      requestAnimationFrame(raf);
    }

    requestAnimationFrame(raf);
  }, []);

  return (
    <div className="flex flex-col w-screen" style={{ maxWidth: "100%" }}>
      <section className="flex w-full h-screen justify-center ">
        <div className="flex flex-col m-auto">
          <p className="">
            Hello,
            <br />
            This is my journey of learning Lenis Scroll & Framer Motion for
            scroll animation UX
          </p>
          <span className="mx-auto me-0 text-end">By: Famaulana</span>
        </div>
      </section>
      <CardYParalax />
      <section className="w-screen h-screen flex justify-center">
        <h1 className="m-auto text-9xl">That was too boring</h1>
      </section>
      <CardXScrollingParalax />
      <section className="w-screen h-screen flex justify-center">
        <h1 className="m-auto text-9xl">Litte bit interesting</h1>
      </section>
      <ZoomSectionParalax />
      <section className="w-screen h-screen bg-white flex justify-center">
        <h1 className="m-auto text-9xl text-black">Ok, i think thats enough</h1>
      </section>
      <section className="w-screen h-screen flex bg-white justify-center">
        <p className="m-auto text-center text-black">
          Thanks for visiting this site
        </p>
      </section>
    </div>
  );
}
